const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

class ConfiguracionAPI extends RESTDataSource {

    constructor(){
        super();
        this.baseURL = serverConfig.configuracion_api_url;
    }

    async rutaInsert(ruta){
        ruta = new Object(JSON.parse(JSON.stringify(ruta)));
        return await  this.post('/rutas/', ruta);
    }

    async rutaUpdate(ruta){
        ruta = new Object(JSON.parse(JSON.stringify(ruta)));
        return await this.put(`/rutas/${ruta.Id}`, ruta);
    }

    async rutaDelete(rutaId){
        return await this.delete(`/rutas/${rutaId}`);
    }

    async rutaGet(rutaid){
        return await this.get(`/rutas/${rutaid}/`);
    }

    async rutaAll(){
        return await this.get(`/rutas/all/`);
    }

    async tarifaInsert(tarifa){
        tarifa = new Object(JSON.parse(JSON.stringify(tarifa)));
        return await  this.post('/tarifas/', tarifa);
    }

    async tarifaUpdate(tarifa){
        tarifa = new Object(JSON.parse(JSON.stringify(tarifa)));
        return await this.put(`/tarifas/${tarifa.Id}`, tarifa);
    }

    async tarifaDelete(tarifaId){
        return await this.delete(`/tarifas/${tarifaId}`);
    }

    async tarifaGet(tarifaId){
        return await this.get(`/tarifas/${tarifaId}`);
        
    }

    async tarifaAll(){
        return await this.get(`/tarifas/all/`);
    }

    async ciudadInsert(ciudad){
        ciudad = new Object(JSON.parse(JSON.stringify(ciudad)));
        return await  this.post('/ciudades', ciudad);
    }

    async ciudadUpdate(ciudad){
        ciudad = new Object(JSON.parse(JSON.stringify(ciudad)));
        return await this.put(`/ciudades/${ciudad.Id}`, ciudad);
    }

    async ciudadDelete(ciudadId){
        return await this.delete(`/ciudades/${ciudadId}`);
    }

    async ciudadGet(ciudadId){
        return await this.get(`/ciudades/${ciudadId}`);
        
    }

    async ciudadAll(){
        return await this.get(`/ciudades/all/`);   
    }
    
    async tipovehiculoInsert(tipovehiculo){
        tipovehiculo = new Object(JSON.parse(JSON.stringify(tipovehiculo)));
        return await  this.post('/tipovehiculos/', tipovehiculo);
    }

    async tipovehiculoUpdate(tipovehiculo){
        tipovehiculo = new Object(JSON.parse(JSON.stringify(tipovehiculo)));
        return await this.put(`/tipovehiculos/${tipovehiculo.Id}`, tipovehiculo);
    }

    async tipovehiculoDelete(tipovehiculoId){
        return await this.delete(`/tipovehiculos/${tipovehiculoId}`);
    }

    async tipovehiculoGet(tipovehiculoId){
        return await this.get(`/tipovehiculos/${tipovehiculoId}`);
    }

    async tipovehiculoAll(){
        return await this.get(`/tipovehiculos/all/`);
    }

    async vehiculoInsert(vehiculo){
        vehiculo = new Object(JSON.parse(JSON.stringify(vehiculo)));
        return await  this.post('/vehiculos/', vehiculo);
    }

    async vehiculoUpdate(vehiculo){
        vehiculo = new Object(JSON.parse(JSON.stringify(vehiculo)));
        return await this.put(`/vehiculos/${vehiculo.Id}`, vehiculo);
    }

    async vehiculoDelete(vehiculoId){
        return await this.delete(`/vehiculos/${vehiculoId}`);
    }

    async vehiculoGet(vehiculoId){
        return await this.get(`/vehiculos/${vehiculoId}`);
    }

    async vehiculoAll(){
        return await this.get(`/vehiculos/all/`);
    }
}

module.exports = ConfiguracionAPI;