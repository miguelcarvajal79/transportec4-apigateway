const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

class SeguridadAPI extends RESTDataSource{

    constructor(){
        super();
        this.baseURL = serverConfig.seguridad_api_url;
    }

    async crearUsuario(usuario){
        usuario = new Object(JSON.parse(JSON.stringify(usuario)));
        return await this.post('/user/', usuario);
    }

    async verUsuario(usuarioId){
        return await this.get(`/user/${usuarioId}`);
    }

    async verUsuarioAll(){
        return await this.get(`/user/usuarios/`);
    }

    async usuarioUpdate(usuario){
        usuario = new Object(JSON.parse(JSON.stringify(usuario)));
        return await this.put(`/user/${usuario.id}/`, usuario);
    }
    
    async usuarioDelete(usuarioId){
        return await this.delete(`/user/${usuarioId}/`);
    }

    async loginUsuario(credentials){
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post('/login/', credentials);
    }
    
    async refreshToken(token){
        token = new Object(JSON.parse(JSON.stringify(token)));
        return await this.post('/refresh/', token);

    }

    async crearRol(rol){        
        rol = new Object(JSON.parse(JSON.stringify(rol)));
        return await this.post*('/rol/', rol);
    }

    async verRol(rolId){
        return await this.get(`/rol/${rolId}`);
    }

    async rolUpdate(rol){
        rol = new Object(JSON.parse(JSON.stringify(rol)));
        return await this.put('/rol', rol);
    }

    async rolDelete(rolId){
        return await this.delete(`/rol/${rolId}`);
    }

    async crearRolOpcion(rolOpcion){
        rolOpcion = new Object(JSON.parse(JSON.stringify(rolOpcion)));
        return await this.post('/rolopcion/', rolOpcion);
    }

    async verRolOpcion(rolOpcionId){
        return await this.get(`/rolopcion/${rolOpcionId}/`);
    }

    async verRolOpcionAll(){
        return await this.get(`/rolopcion/rolopciones/`);
    }

    async rolOpcionUpdate(rolOpcion){
        rolOpcion = new Object(JSON.parse(JSON.stringify(rolOpcion)));
        return await this.put('/rolopcion/', rolOpcion);
    }

    async rolOpcionDelete(rolOpcionId){
        return await this.delete(`/rolopcion/${rolOpcionId}/`);
    }

    async crearOpcion(opcion){
        opcion = new Object(JSON.parse(JSON.stringify(opcion)));
        return await this.post*('/opcion/', opcion);
    }

    async verOpcion(opcionId){
        return await this.get(`/opcion/${opcionId}`);
    }

    async opcionUpdate(opcion){
        opcion = new Object(JSON.parse(JSON.stringify(opcion)));
        return await this.put('/opcion', opcion);
    }

    async opcionDelete(opcionId){
        return await this.delete(`/opcion/${opcionId}`);
    }
}
module.exports = SeguridadAPI;