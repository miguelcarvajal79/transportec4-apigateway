const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

class VentasAPI extends RESTDataSource{

    constructor(){
        super();
        this.baseURL = serverConfig.ventas_api_url;
    }

    async ventaInsert(venta){
        venta = new Object(JSON.parse(JSON.stringify(venta)));
        return await  this.post('/venta/', venta);
    }

    async ventaUpdate(venta){
        venta = new Object(JSON.parse(JSON.stringify(venta)));
        return await this.put(`/venta/${venta.id}/`, venta);
    }

    async ventaDelete(ventaId){
        return await this.delete(`/venta/${ventaId}/`);
    }

    async ventaGet(ventaId){
        return await this.get(`/venta/${ventaId}/`);
    }

    async ventaAll(){
        return await this.get(`/venta/ventas/`);
    }

    async ventaUsuarioGet(username){
        return await this.get(`/venta/ventas/${username}`);
    }
}
module.exports = VentasAPI;