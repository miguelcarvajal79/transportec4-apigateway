const { ApolloServer } = require('apollo-server');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const ConfiguracionAPI = require('./dataSources/configuracion_api');
const SeguridadAPI = require('./dataSources/seguridad_api');
const VentasAPI = require('./dataSources/ventas_api');
const authentication = require('./utils/autenticacion');

const server = new ApolloServer({
    context: authentication,
    typeDefs,
    resolvers,
    dataSources: () => ({
        seguridadAPI: new SeguridadAPI(),
        configuracionAPI: new ConfiguracionAPI(),
        ventasAPI: new VentasAPI(),
    }),
    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});