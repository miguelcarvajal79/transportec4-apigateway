const ciudadResolver = {
    Query: {
        ciudadById: (_, { ciudadid }, { dataSources }) => {
            return dataSources.configuracionAPI.ciudadGet(ciudadid)
        },
        ciudadAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.configuracionAPI.ciudadAll()
            else
                return null
        },
    },
    Mutation: {
        createCiudad: async (_, { ciudadInput }, { dataSources }) => {
            const ciudadEntrada = {
                id: ciudadInput.id,
                nombre: ciudadInput.nombre,
            }
            return await dataSources.configuracionAPI.ciudadInsert(ciudadEntrada);
        },

        updateCiudad: async (_, { ciudadInput }, { dataSources }) => {
            const ciudadEntrada = {
                id: ciudadInput.id,
                nombre: ciudadInput.nombre,
            }
            return await dataSources.configuracionAPI.ciudadUpdate(ciudadEntrada);
        },

        deleteCiudad: async (_, { ciudadid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.ciudadDelete(ciudadid);
        }
    }
};

module.exports = ciudadResolver;