const ciudadResolver = require('./ciudad_resolver');
const opcionResolver = require('./opcion_resolver');
const rolResolver = require('./rol_resolver');
const rolopcionResolver = require('./rolopcion_resolver');
const rutaResolver = require('./ruta_resolver');
const tarifaResolver = require('./tarifa_resolver');
const tipoVehiculoResolver = require('./tipovehiculo_resolver');
const usuarioResolver = require('./usuario_resolver');
const vehiculoResolver = require('./vehiculo_resolver');
const ventaResolver = require('./venta_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(usuarioResolver,rutaResolver,rolopcionResolver,ciudadResolver,opcionResolver,tarifaResolver,ventaResolver,rolResolver,tipoVehiculoResolver,vehiculoResolver);

module.exports = resolvers;