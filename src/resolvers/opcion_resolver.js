const opcionResolver = {
    Query: {
        opcionById: (_, { opcionid }, { dataSources }) => {
            return dataSources.configuracionAPI.opcionGet(opcionid)
        },
    },
    Mutation: {
        createOpcion: async (_, { opcionInput }, { dataSources }) => {
            const opcionEntrada = {
                id: opcionInput.id,
                nombre: opcionInput.nombre,
                estado: opcionInput.estado,
            }
            return await dataSources.configuracionAPI.opcionInsert(opcionEntrada);
        },

        updateOpcion: async (_, { opcionInput }, { dataSources }) => {
            const opcionEntrada = {
                id: opcionInput.id,
                nombre: opcionInput.nombre,
                estado: opcionInput.estado,
            }
            return await dataSources.configuracionAPI.opcionUpdate(opcionEntrada);
        },

        deleteOpcion: async (_, { opcionid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.opcionDelete(opcionid);
        }
    }
};

module.exports = opcionResolver;