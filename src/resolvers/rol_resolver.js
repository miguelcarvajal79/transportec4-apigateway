const rolResolver = {
    Query: {
        rolById: (_, { rolid }, { dataSources }) => {
            return dataSources.configuracionAPI.rolGet(rolid)
        },
    },
    Mutation: {
        createRol: async (_, { rolInput }, { dataSources }) => {
            const rolEntrada = {
                id: rolInput.id,
                nombre: rolInput.nombre,
                estado: rolInput.estado,
            }
            return await dataSources.configuracionAPI.rolInsert(rolEntrada);
        },

        updateRol: async (_, { rolInput }, { dataSources }) => {
            const rolEntrada = {
                id: rolInput.id,
                nombre: rolInput.nombre,
                estado: rolInput.estado,
            }
            return await dataSources.configuracionAPI.rolUpdate(rolEntrada);
        },

        deleteRol: async (_, { rolid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.rolDelete(rolid);
        }
    }
};

module.exports = rolResolver;