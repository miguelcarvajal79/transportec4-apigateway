const rolopcionResolver = {
    Query: {
        rolopcionById: (_, { rolopcionid }, { dataSources }) => {
            return dataSources.seguridadAPI.verRolOpcion(rolopcionid)
        },
        rolopcionAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.seguridadAPI.verRolOpcionAll()
            else
                return null
        },
    },
    Mutation: {
        createRolOpcion: async (_, { rolopcionInput }, { dataSources }) => {
            const rolopcionEntrada = {
                rolid: rolopcionInput.rolid,
                opcionid: rolopcionInput.opcionid,
            }
            return await dataSources.seguridadAPI.crearRolOpcion(rolopcionEntrada);
        },

        updateRolOpcion: async (_, { rolopcionInput }, { dataSources }) => {
            const rolopcionEntrada = {
                rolid: rolopcionInput.rolid,
                opcionid: rolopcionInput.opcionid,
            }
            return await dataSources.seguridadAPI.rolOpcionUpdate(rolopcionEntrada);
        },

        deleteRolOpcion: async (_, { rolopcionid }, { dataSources }) => {            
            return await dataSources.seguridadAPI.rolOpcionDelete(rolopcionid);
        }
    }
};

module.exports = rolopcionResolver;