const rutaResolver = {
    Query: {
        rutaById: (_, { rutaid }, { dataSources }) => {
            return dataSources.configuracionAPI.rutaGet(rutaid)
        },
        rutaAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.configuracionAPI.rutaAll()
            else
                return null
        },
    },
    Mutation: {
        createRuta: async (_, { rutaInput }, { dataSources }) => {
            const rutaEntrada = {
                id: rutaInput.id,
                origen: rutaInput.origen,
                destino: rutaInput.destino,
                fecha: rutaInput.fecha,
                estado: rutaInput.estado,
            }
            return await dataSources.configuracionAPI.rutaInsert(rutaEntrada);
        },

        updateRuta: async (_, { rutaInput }, { dataSources }) => {
            const rutaEntrada = {
                id: rutaInput.id,
                origen: rutaInput.origen,
                destino: rutaInput.destino,
                fecha: rutaInput.fecha,
                estado: rutaInput.estado,
            }
            return await dataSources.configuracionAPI.rutaUpdate(rutaEntrada);
        },

        deleteRuta: async (_, { rutaid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.rutaDelete(rutaid);
        }
    }
};

module.exports = rutaResolver;