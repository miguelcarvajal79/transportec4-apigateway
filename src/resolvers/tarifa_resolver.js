const tarifaResolver = {
    Query: {
        tarifaById: (_, { tarifaid }, { dataSources }) => {
            return  dataSources.configuracionAPI.tarifaGet(tarifaid)
        },
        tarifaAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.configuracionAPI.tarifaAll()
            else
                return null
        },
    },
    Mutation: {
        createTarifa: async (_, { tarifaInput }, { dataSources }) => {
            const tarifaEntrada = {
                id: tarifaInput.id,
                vehiculo: tarifaInput.vehiculo,
                ruta: tarifaInput.ruta,
                costo: tarifaInput.costo,
                estado: tarifaInput.estado,
            }
            return await dataSources.configuracionAPI.tarifaInsert(tarifaEntrada);
        },

        updateTarifa: async (_, { tarifaInput }, { dataSources }) => {
            const tarifaEntrada = {
                id: tarifaInput.id,
                vehiculo: tarifaInput.vehiculo,
                ruta: tarifaInput.ruta,
                costo: tarifaInput.costo,
                estado: tarifaInput.estado
            }
            return await dataSources.configuracionAPI.tarifaUpdate(tarifaEntrada);

        },

        deleteTarifa: async (_, { tarifaid }, { dataSources }) => {
            return await dataSources.configuracionAPI.tarifaDelete(tarifaid);
        }
    }
};

module.exports = tarifaResolver;