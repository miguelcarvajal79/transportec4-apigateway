const tipoVehiculoResolver = {
    Query: {
        tipoVehiculoById: (_, { tipovehiculoid }, { dataSources }) => {
            return dataSources.configuracionAPI.tipovehiculoGet(tipovehiculoid)
        },
        tipoVehiculoAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.configuracionAPI.tipovehiculoAll()
            else
                return null
        },
    },
    Mutation: {
        createTipoVehiculo: async (_, { tipoVehiculoInput }, { dataSources }) => {
            const tipoVehiculoEntrada = {
                id: tipoVehiculoInput.id,
                codigo: tipoVehiculoInput.codigo,
                nombre: tipoVehiculoInput.nombre,
                descripcion: tipoVehiculoInput.descripcion,
                estado: tipoVehiculoInput.estado,
            }
            return await dataSources.configuracionAPI.tipovehiculoInsert(tipoVehiculoEntrada);
        },

        updateTipoVehiculo: async (_, { tipoVehiculoInput }, { dataSources }) => {
            const tipoVehiculoEntrada = {
                id: tipoVehiculoInput.id,
                codigo: tipoVehiculoInput.codigo,
                nombre: tipoVehiculoInput.nombre,
                descripcion: tipoVehiculoInput.descripcion,
                estado: tipoVehiculoInput.estado,
            }
            return await dataSources.configuracionAPI.tipovehiculoUpdate(tipoVehiculoEntrada);
        },

        deleteTipoVehiculo: async (_, { tipovehiculoid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.tipovehiculoDelete(tipovehiculoid);
        }
    }
};

module.exports = tipoVehiculoResolver;