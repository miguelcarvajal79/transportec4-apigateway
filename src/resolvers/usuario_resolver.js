const usuarioResolver = {
    Query: {
        userDetailById: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.seguridadAPI.verUsuario(usuarioid)
            else
                return null
        },
        userDetallAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.seguridadAPI.verUsuarioAll()
            else
                return null
        },
    },
    Mutation: {
        signUpUser: async (_, { userInput }, { dataSources }) => {
            const usuarioInput = {
                id: userInput.id,
                username: userInput.username,
                password: userInput.password,
                tipoidentificacion: userInput.tipoidentificacion,
                nombre: userInput.nombre,
                apellido: userInput.apellido,
                celular: userInput.celular,
                email: userInput.email,
                rolid: userInput.rolid,
                estado: userInput.estado,
            }
            return await dataSources.seguridadAPI.crearUsuario(usuarioInput);
        },

        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.seguridadAPI.loginUsuario(credentials),

        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.seguridadAPI.refreshToken(refresh),

        updateUsuario: async (_, { usuario }, { dataSources }) => {
            const usuarioInput = {
                id: usuario.id,
                username: usuario.username,
                password: usuario.password,
                tipoidentificacion: usuario.tipoidentificacion,
                nombre: usuario.nombre,
                apellido: usuario.apellido,
                celular: usuario.celular,
                email: usuario.email,
                rolid: usuario.rolid,
                estado: usuario.estado,
            }
            return await dataSources.seguridadAPI.usuarioUpdate(usuarioInput);
        },

        deleteUsuario: async (_, { usuarioid }, { dataSources }) => {            
            return await dataSources.seguridadAPI.usuarioDelete(usuarioid);
        }
    }
};

module.exports = usuarioResolver;