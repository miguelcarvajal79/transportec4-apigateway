const vehiculoResolver = {
    Query: {
        vehiculoById: (_, { vehiculoid }, { dataSources }) => {
            return dataSources.configuracionAPI.vehiculoGet(vehiculoid)
        },
        vehiculoAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.configuracionAPI.vehiculoAll()
            else
                return null
        },
    },
    Mutation: {
        createVehiculo: async (_, { vehiculoInput }, { dataSources }) => {
            const vehiculoEntrada = {
                id: vehiculoInput.id,
                matricula: vehiculoInput.matricula,
                modelo: vehiculoInput.modelo,
                color: vehiculoInput.color,
                estado: vehiculoInput.estado,
                disponibilidad: vehiculoInput.disponibilidad,
                fecha: (new Date()).toISOString(),
                empresa: vehiculoInput.empresa,
                tipovehiculo: vehiculoInput.tipovehiculo,
                usuario: vehiculoInput.usuario,
            }
            return await dataSources.configuracionAPI.vehiculoInsert(vehiculoEntrada);
        },

        updateVehiculo: async (_, { vehiculoInput }, { dataSources }) => {
            const vehiculoEntrada = {
                id: vehiculoInput.id,
                matricula: vehiculoInput.matricula,
                modelo: vehiculoInput.modelo,
                color: vehiculoInput.color,
                estado: vehiculoInput.estado,
                disponibilidad: vehiculoInput.disponibilidad,
                fecha: (new Date()).toISOString(),
                empresa: vehiculoInput.empresa,
                tipovehiculo: vehiculoInput.tipovehiculo,
                usuario: vehiculoInput.usuario,
            }
            return await dataSources.configuracionAPI.vehiculoUpdate(vehiculoEntrada);
        },

        deleteVehiculo: async (_, { vehiculoid }, { dataSources }) => {            
            return await dataSources.configuracionAPI.vehiculoDelete(vehiculoid);
        }
    }
};

module.exports = vehiculoResolver;