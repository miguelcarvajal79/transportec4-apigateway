const ventaResolver = {
    Query: {
        ventaById: (_, { ventaid }, { dataSources }) => {
            return dataSources.ventasAPI.ventaGet(ventaid)
        },
        ventaAll: (_, { usuarioid }, { dataSources, userIdToken }) => {
            if (usuarioid == userIdToken)
                return dataSources.ventasAPI.ventaAll()
            else
                return null
        },
    },
    Mutation: {
        createVenta: async (_, { ventaInput }, { dataSources }) => {
            const ventaEntrada = {
                id: ventaInput.id,
                username: ventaInput.username,
                vehiculoid: ventaInput.vehiculoid,
                rutaid: ventaInput.rutaid,
                pasajeros: ventaInput.pasajeros,
                estado: ventaInput.estado,
            }
            return await dataSources.ventasAPI.ventaInsert(ventaEntrada);
        },

        updateVenta: async (_, { ventaInput }, { dataSources }) => {
            const ventaEntrada = {
                id: ventaInput.id,
                username: ventaInput.username,
                vehiculoid: ventaInput.vehiculoid,
                rutaid: ventaInput.rutaid,
                pasajeros: ventaInput.pasajeros,
                estado: ventaInput.estado,
            }
            return await dataSources.ventasAPI.ventaUpdate(ventaEntrada);
        },

        deleteVenta: async (_, { ventaid }, { dataSources }) => {            
            return await dataSources.ventasAPI.ventaDelete(ventaid);
        }
    }
};

module.exports = ventaResolver;