module.exports = {
    seguridad_api_url: 'https://transportec4-msseguridad.herokuapp.com',
    configuracion_api_url: 'https://transportec4-msconfiguracion.herokuapp.com',
    ventas_api_url: 'https://transportec4-msventas.herokuapp.com',
};