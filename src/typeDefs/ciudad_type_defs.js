const { gql } = require('apollo-server');

const ciudadTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input CiudadInput {
        id: Int!
        nombre: String!
    }

    type Ciudad {
        id: Int!
        nombre: String!
    }

    type Mutation {
        createCiudad(ciudadInput: CiudadInput): Ciudad!
        updateCiudad(ciudadInput: CiudadInput!): Ciudad!
        deleteCiudad(ciudadid: Int!): String!
    }

    type Query {
        ciudadById(ciudadid: Int!): Ciudad!
        ciudadAll: [Ciudad!]
    }
`;

module.exports = ciudadTypeDefs;