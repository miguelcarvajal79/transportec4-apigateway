const ciudadTypeDefs = require('./ciudad_type_defs');
const opcionTypeDefs = require('./opcion_type_defs');
const rolopcionTypeDefs = require('./rolopcion_type_defs');
const rutaTypeDefs = require('./ruta_type_defs');
const tarifaTypeDefs = require('./tarifa_type_defs');
const usuarioTypeDefs = require('./usuario_type_defs');
const ventaTypeDefs = require('./venta_type_defs');
const rolTypeDefs = require('./rol_type_defs');
const tipoVehiculoTypeDefs = require('./tipovehiculo_type_defs');
const vehiculoTypeDefs = require('./vehiculo_type_defs');

const schemasArrays = [usuarioTypeDefs,rutaTypeDefs,rolopcionTypeDefs,ciudadTypeDefs,opcionTypeDefs,tarifaTypeDefs,ventaTypeDefs,rolTypeDefs,tipoVehiculoTypeDefs,vehiculoTypeDefs];

module.exports = schemasArrays;