const { gql } = require('apollo-server');

const opcionTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input OpcionInput {
        id: Int!
        nombre: String!
        estado: String!
    }

    type Opcion {
        id: Int!
        nombre: String!
        estado: String!
    }

    type Mutation {
        createOpcion(opcionInput :OpcionInput): Opcion!
        updateOpcion(opcionInput: OpcionInput!): Opcion!
        deleteOpcion(opcionid: Int!): String!
    }

    type Query {
        opcionById(opcionid: Int!): Opcion!
        opcionAll: [Opcion!]
    }
`;

module.exports = opcionTypeDefs;