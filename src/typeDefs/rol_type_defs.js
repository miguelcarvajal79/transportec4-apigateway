const { gql } = require('apollo-server');

const rolTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input RolInput {
        id: Int!
        nombre: String!
        estado: String!
    }

    type Rol {
        id: Int!
        nombre: String!
        estado: String!
    }

    type Mutation {
        createRol(rolInput: RolInput): Rol!
        updateRol(rolInput: RolInput!): Rol!
        deleteRol(rolid: Int!): String!
    }

    type Query {
        rolById(rolid: Int!): Rol!
        rolAll: [Rol!]
    }
`;

module.exports = rolTypeDefs;