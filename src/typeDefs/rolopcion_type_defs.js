const { gql } = require('apollo-server');

const rolopcionTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input RolOpcionInput {
        rolid: Int!
        opcionid: Int!
    }

    type RolOpcion {
        rolid: Int
        opcionid: Int
    }

    type Mutation {
        createRolOpcion(rolopcionInput :RolOpcionInput): RolOpcion
        updateRolOpcion(rolopcionInput: RolOpcionInput!): Message!
        deleteRolOpcion(rolopcionid: Int!): Message!
    }

    type Query {
        rolopcionById(rolopcionid: Int!): RolOpcion!
        rolopcionAll: [RolOpcion!]
    }
`;

module.exports = rolopcionTypeDefs;