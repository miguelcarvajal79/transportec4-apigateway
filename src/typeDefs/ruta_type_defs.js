const { gql } = require('apollo-server');

const rutaTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input RutaInput {
        id: Int!
        origen: Int!
        destino: Int!
        fecha: String!
        estado: String!
    }

    type Ruta {
        id: Int!
        origen: Int!
        destino: Int!
        fecha: String!
        estado: String!
    }

    type Mutation {
        createRuta(rutaInput :RutaInput): Ruta!
        updateRuta(rutaInput: RutaInput!): Ruta!
        deleteRuta(rutaid: Int!): String!
    }

    type Query {
        rutaById(rutaid: Int!): Ruta!
        rutaAll: [Ruta!]
    }
`;

module.exports = rutaTypeDefs;