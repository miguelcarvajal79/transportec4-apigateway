const { gql } = require('apollo-server');

const tarifaTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input TarifaInput {
        id: Int!
        vehiculo: Int!
        ruta: Int!
        costo: Int!
        estado: String!
    }

    type Tarifa {
        id: Int!
        vehiculo: Int!
        ruta: Int!
        costo: Int!
        estado: String!
    }

    type Mutation {
        createTarifa(tarifaInput: TarifaInput): Tarifa!
        updateTarifa(tarifaInput: TarifaInput!): Tarifa!
        deleteTarifa(tarifaid: Int!): String!
    }

    type Query {
        tarifaById(tarifaid: Int!): Tarifa!
        tarifaAll: [Tarifa!]
    }
`;

module.exports = tarifaTypeDefs;