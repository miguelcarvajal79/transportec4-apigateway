const { gql } = require('apollo-server');

const tipoVehiculoTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input TipoVehiculoInput {
        id: Int!
        codigo: String!
        nombre: String!
        descripcion: String!
        estado: String!
    }

    type TipoVehiculo {
        id: Int!
        codigo: String!
        nombre: String!
        descripcion: String!
        estado: String!    
    }

    type Mutation {
        createTipoVehiculo(tipoVehiculoInput: TipoVehiculoInput): TipoVehiculo!
        updateTipoVehiculo(tipoVehiculoInput: TipoVehiculoInput!): TipoVehiculo!
        deleteTipoVehiculo(tipovehiculoid: Int!): String!
    }

    type Query {
        tipoVehiculoById(tipovehiculoid: Int!): TipoVehiculo!
        tipoVehiculoAll: [TipoVehiculo!]
    }
`;

module.exports = tipoVehiculoTypeDefs;