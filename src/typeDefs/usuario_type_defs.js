const { gql } = require('apollo-server');

const usuarioTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    type Message{
        Respuesta: String!
    }

    input CredentialsInput {
        username: String!
        password: String!
    }

    input SignUpInput {
        id: Int!
        username: String!
        password: String!
        tipoidentificacion: String!
        nombre: String!
        apellido: String!
        celular: Float!
        email: String!
        rolid: Int!
        estado: String!
    }

    type UserDetail {
        id: Int!
        username: String!
        password: String!
        tipoidentificacion: String!
        nombre: String!
        apellido: String!
        celular: Float!
        email: String!
        rolid: Int!
        estado: String!
    }

    type Mutation {
        signUpUser(userInput :SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
        updateUsuario(usuario: SignUpInput!): Message!
        deleteUsuario(usuarioid: Int!): Message!
    }

    type Query {
        userDetailById(usuarioid: Int!): UserDetail!
        userDetallAll: [UserDetail!]
    }
`;

module.exports = usuarioTypeDefs;