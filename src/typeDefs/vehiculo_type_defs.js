const { gql } = require('apollo-server');

const vehiculoTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input VehiculoInput {
        id: Int!
        matricula: String!
        modelo: String!
        color: String!
        estado: String!
        disponibilidad: String!
        fecha: String!
        empresa: String!
        tipovehiculo: Int!
        usuario: Int!
    }

    type Vehiculo {
        id: Int!
        matricula: String!
        modelo: String!
        color: String!
        estado: String!
        disponibilidad: String!        
        fecha: String!
        empresa: String!
        tipovehiculo: Int!
        usuario: Int!
    }

    type Mutation {
        createVehiculo(vehiculoInput: VehiculoInput): Vehiculo!
        updateVehiculo(vehiculoInput: VehiculoInput!): Vehiculo!
        deleteVehiculo(vehiculoid: Int!): String!
    }

    type Query {
        vehiculoById(vehiculoid: Int!): Vehiculo!
        vehiculoAll: [Vehiculo!]
    }
`;

module.exports = vehiculoTypeDefs;