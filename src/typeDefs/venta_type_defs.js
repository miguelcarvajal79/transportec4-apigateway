const { gql } = require('apollo-server');

const ventaTypeDefs = gql `
    type Message{
        Respuesta: String!
    }

    input VentaInput {
        id: Int!
        username: String!
        vehiculoid: Int!
        rutaid: Int!
        pasajeros: Int!
        estado: String!
    }

    type Venta {
        id: Int!
        username: String!
        vehiculoid: Int!
        rutaid: Int!
        pasajeros: Int!
        estado: String!
    }

    type Mutation {
        createVenta(ventaInput: VentaInput): Int!
        updateVenta(ventaInput: VentaInput!): Message!
        deleteVenta(ventaid: Int!): Message!
    }

    type Query {
        ventaById(ventaid: Int!): Venta!
        ventaAll: [Venta!]
    }
`;

module.exports = ventaTypeDefs;